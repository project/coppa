<?php


/**
 * @file
 * COPPA module admin pages and forms.
 */

/**
 * Menu Callback: COPPA admin forms.
 */
function coppa_manage_page($path = NULL) {
  $bcrumb = drupal_get_breadcrumb();
  $bcrumb[] = l(t('COPPA'), 'admin/user/coppa/add');
  switch($path) {
    case 'add':
      $output = drupal_get_form('coppa_set_parents');
      $bcrumb[] = l(t('Add'), 'admin/user/coppa/add');
      break;
    case 'manage':
    default: 
      $manage = coppa_manage_relationships();
      $bcrumb[] = l(t('Manage'), 'admin/user/coppa/manage');
      if (!empty($manage)) {
        $output = drupal_get_form('coppa_manage_relationships');
      }
      else {
        $output = t('No existing COPPA relationships.');
      }
      break;
  }
  
  drupal_set_title(t('COPPA'));
  drupal_set_breadcrumb($bcrumb);
  return $output;
}

/**
 * Informational page to prompt user about COPPA requirements.
 */
function coppa_info() {
  $content = '<p>'. t('COPPA compliance is required on this site. Please remind your learning coach to sign your COPPA forms.') .'</p>';
  return $content;
}

/**
 * Menu Callback: Sign COPPA form.
 */
function coppa_sign() {
  global $user;

  $form = array(
    '#theme' => 'coppa_sign',
  );

  $result = db_query("SELECT DISTINCT u.uid, u.name, c.status, pv.value, pv.fid 
                      FROM {users} u, {coppa} c, {profile_fields} pf, {profile_values} pv 
                      WHERE c.cid=u.uid AND pv.uid = u.uid AND pf.fid = pv.fid AND pf.name = 'profile_dob' AND c.pid = %d", $user->uid);

  $form['children'] = array('#tree' => TRUE);

  $weight = 1;
  while ($r = db_fetch_object($result)) {
    $dob = unserialize($r->value);

    $coppa_needed = coppa_needed($r->uid);
    $coppa_comply = coppa_is_compliant($r->uid);

    $form['children'][$r->uid]['tracker'] = array('#type' => 'markup', '#value' => l($r->name, 'tracker/'. $r->uid));
    $form['children'][$r->uid]['dob'] = array('#type' => 'markup', '#value' => $dob['month'] .'/'. $dob['day'] .'/'. $dob['year']);
    if ($coppa_needed && !$coppa_comply) {
      // we need to render the field unchecked
      $checked = 0;
      $disabled = 0;
    }
    elseif ($coppa_needed && $coppa_comply) {
      // we need to render the field checked and disabled
      $checked = 1;
      $disabled = 1;
    }
    else {
      // unchecked disabled
      $checked = 1;
      $disabled = 1;
    }
    $form['children'][$r->uid]['status'] = array('#type' => 'checkbox', 
                                                '#default_value' => $checked, 
                                                '#disabled' => $disabled);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Confirm COPPA'));

  if (count($form['children']) == 1) {
    return array('message' => array('#type' => 'message', '#value' => t('You do not have any children associated to you.')));
  }
  else {
    return $form;
  }
}

/**
 * Submit handler for coppa_sign.
 */
function coppa_sign_submit($form, &$form_state) {
  global $user;
  foreach ($form_state['values']['children'] as $cid => $status) {
    if ($status['status'] == 1) {
      $result = db_query("SELECT status FROM {coppa} WHERE pid= %d AND cid= %d", $user->pid, $cid);
      $r = db_fetch_object($result);
      if ($r->status == 0) {
        $result = db_query("UPDATE {coppa} SET status = %d WHERE pid = %d AND cid = %d", time(), $user->uid, $cid);
      }
    }
  }
}

/**
 * Theme the COPPA sign page/form.
 */
function theme_coppa_sign($form) {
  $rows = array();
  if (!isset($form['children'])) {
    $output = $form['message']['#value'];
  }
  else {
    $header = array(t('User History'), t('Date of Birth'), t('COPPA Status'));
  
    foreach (element_children($form['children']) as $uid) {
      unset($row);
      $row[] = drupal_render($form['children'][$uid]['tracker']);
      $row[] = drupal_render($form['children'][$uid]['dob']);
      unset($form['children'][$uid]['status']['#title']);
      $row[] = drupal_render($form['children'][$uid]['status']);
      $rows[] = $row;
    }
  
    $output = theme('table', $header, $rows, array('id' => 'coppa-sign'));
    $output .= '<br />';
    $output .= drupal_render($form['submit']);

    $output .= drupal_render($form['form_id']);

    $output .= drupal_render($form['form_token']);
  }
  return $output;

}

/**
 * Form for COPPA admin settings.
 */
function coppa_admin_settings() {
  $roles = user_roles(TRUE);
  
  $selected = variable_get('coppa_roles', 'a:0:{}');
  // $selected = unserialize($selected);
  
  $form['coppa_roles'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Roles that will require COPPA compliance'),
    '#default_value' => $selected,
    '#size' => min(12, count($roles)),
    '#options' => $roles,
    '#description' => t('Please be aware that setting this for unintended roles can lock users out of the system.'),
  );

  // Core profile module vs. content_profile
  $options = array(
    'profile' => t('Drupal core <em>profile</em> module'),
  );
  if (module_exists('content_profile')) {
    $options['content_profile'] = t('CCK <a href="!url">Content Profile</a> module', array('!url' => url('http://drupal.org/project/content_profile')));
  }
  $form['coppa_dob_field_type'] = array(
    '#type' => 'radios',
    '#title' => t('User date-of-birth field module'),
    '#options' => $options,
    '#description' => t("The module used to collect a user's dob."),
    '#default_value' => variable_get('coppa_dob_field_type', 'profile'),
  );
  $form['coppa_dob_field'] = array(
    '#type' => 'textfield',
    '#title' => t('User date-of-birth field name'),
    '#description' => t('Typically, if using the <em>profile</em> module, this is profile_dob, and if using a CCK field via the content_profile module, it will be prefixed with <em>field_</em>, such as <em>field_dob</em>.'),
    '#default_value' => variable_get('coppa_dob_field', 'profile_dob'),
  );

  return system_settings_form($form);
}

/**
 * Form for admin/coppa/set-parents
 */
function coppa_set_parents() {
  $form = array(
    '#theme' => 'coppa_set_parents',
  );

  $form['rows'] = array('#tree' => TRUE);
  
  for ($i = 0; $i < COPPA_ADD_ELEMENTS; $i++) {
    $form['rows'][$i]['parent'] = array(
      '#type' => 'textfield',
      '#title' => t('Parent'),
      '#maxlength' => 60,
      '#size' => 32,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => ''
    );
    $form['rows'][$i]['child'] = array(
      '#type' => 'textfield',
      '#title' => t('Child'),
      '#maxlength' => 60,
      '#size' => 32,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => ''
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Set Parent to Child Relationships'));
  return $form;
}

/**
 * Form validation for coppa_set_parents_submit
 */
function coppa_set_parents_validate($form, &$form_state) {
  foreach ($form_state['values']['rows'] as $rid => $row) {
    if (!empty($row['parent'])) {
      if (!empty($row['child'])) {
        // make sure both are valid usernames (is there a better way than user_load?)
        $parent = user_load(array('name' => $row['parent']));
        $child = user_load(array('name' => $row['child']));
        
        if (empty($parent->uid)) {
          form_set_error("$rid][parent", t('Parent name %parentname is invalid', array('%parentname' => $row['parent'])));
        }
        else {
          if (empty($child->uid)) {
            form_set_error("$rid][child", t('Child name %childname is invalid', array('%childname' => $row['child'])));
          }
          else {
            if ($parent->uid == $child->uid) {
              form_set_error($rid, t('Parent and Child (%name) cannot be the same person', array('%name' => $child->name)));
            }
            else {
              // both are valid users let's see if we have an entry in coppa
              $result = db_query('SELECT cid, pid, status FROM {coppa} WHERE cid = %d AND pid = %d', $child->uid, $parent->uid);
              $coppa = db_fetch_object($result);
              if (isset($coppa->cid) && isset($coppa->pid)) {
                form_set_error($rid, t('Relationship %parent to %child already exists', array('%parent' => $parent->name, '%child' => $child->name)));
              }
              else {
                // parent and child are valid users and no existing record in coppa
                // validation is good
              }
            }
          }
        }
      }
      else {
        form_set_error("$rid][child", t('Parent (%name) and Child must both be completed', array('%name' => $row['parent'])));
      }
    }
    else {  // parent empty
      if (!empty($row['child'])) {
        form_set_error("$rid][child", t('Parent and Child (%name) fields must both be completed', array('%name' => $row['child'])));
      }
      else {
        // empty row... do nothing
      }
    }
  }
}

/**
 * Form submission handler for coppa/set-parents
 */
function coppa_set_parents_submit($form, &$form_state) {
  
  $inserted = array();  // keep track of what we've added to avoid double inserts (we didn't check for that in validate)
  
  // form has gone through validation
  foreach ($form_state['values']['rows'] as $rid => $row) {
    if (!empty($row['child']) && !empty($row['parent'])) {
      $child = user_load(array('name' => $row['child']));
      $parent = user_load(array('name' => $row['parent']));
      
      if (!isset($inserted[$parent->uid])) {
        $inserted[$parent->uid] = array();
      }
      
      if (!in_array($child->uid, $inserted[$parent->uid])) {
        db_query('INSERT INTO {coppa} (cid, pid, status) values(%d, %d, %d)', $child->uid, $parent->uid, 0);
        drupal_set_message(t('Added %cname as child of %pname', array('%cname' => $child->name, '%pname' => $parent->name)));
        $inserted[$parent->uid][] = $child->uid;
      }
      else {
        // duplicate entry no big thing... just don't try to double insert
      }
    }
  }
}

/**
 * Theme for coppa_set_parents to display in tabular format
 */
function theme_coppa_set_parents($form) {
  $rows = array();
  
  $header = array(t('Parent'), t('Child'));
  
  foreach (element_children($form['rows']) as $r) {
    unset($row);

    unset($form['rows'][$r]['parent']['#title']);
    $row[] = drupal_render($form['rows'][$r]['parent']);

    unset($form['rows'][$r]['child']['#title']);
    $row[] = drupal_render($form['rows'][$r]['child']);

    $rows[] = $row;
  }
  
  $output = theme('table', $header, $rows, array('id' => 'coppa-set-parents'));
  $output .= '<br />';
  $output .= drupal_render($form['submit']);

  $output .= drupal_render($form['form_id']);

  $output .= drupal_render($form['form_token']);

  return $output;
}

/**
 * form handler for coppa/manage-relationships
 */
function coppa_manage_relationships() {
  $form = array(
    '#theme' => 'coppa_manage_relationships',
  );
  $date_format = 'n j Y'; // m/d/y  
  $result = db_query("SELECT cu.uid as cid, cu.name as cname, pu.uid as pid, pu.name as pname, pv.value as dob, c.status 
                      FROM {users} cu
                      JOIN {coppa} c ON cu.uid = c.cid
                      JOIN {users} pu ON pu.uid = c.pid 
                      LEFT JOIN {profile_values} pv ON pv.uid = c.cid 
                      LEFT JOIN {profile_fields} pf ON pv.fid = pf.fid");
    

  while ($r = db_fetch_object($result)) {
    $r->dob = unserialize($r->dob);

    if (!isset($form[$r->pid])) {
      $form[$r->pid] = array('#tree' => TRUE);
    }
    if (!isset($form[$r->pid][$r->cid])) {
      $form[$r->pid][$r->cid] = array('#tree' => TRUE);
    }
    
    $form[$r->pid][$r->cid]['parent'] = array('#type' => 'markup', '#value' => l($r->pname, 'user/'. $r->pid));
    $form[$r->pid][$r->cid]['child'] = array('#type' => 'markup', '#value' => l($r->cname, 'user/'. $r->cid));

    if (!empty($r->dob)) {
      $form[$r->pid][$r->cid]['dob'] = array('#type' => 'markup', '#value' => $r->dob['month'] .'/'. $r->dob['day'] .'/'. $r->dob['year']);
    }
    else {
      $form[$r->pid][$r->cid]['dob'] = array('#type' => 'markup', '#value' => t('No DOB on record'));
    }  

    switch ($r->status) {
      case 0:
        if (coppa_needed($r->cid)) {
          $message = t('COPPA Needed');
        }
        else {
          $message = t('COPPA Unnecessary');
        }
        $form[$r->pid][$r->cid]['status'] = array('#type' => 'markup', '#value' => $message);
        break;
      case 1:
        $form[$r->pid][$r->cid]['status'] = array('#type' => 'markup', '#value' => t('N/A'));
        break;
      default:
        $form[$r->pid][$r->cid]['status'] = array('#type' => 'markup', '#value' => t('Compliant on: %date', array('%date' => date($date_format, $r->status))));
        break;
    }
    
    $form[$r->pid][$r->cid]['remove'] = array('#type' => 'checkbox', '#default_value' => 0);
  }

  if (!empty($form)) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Remove Selected Parent to Child Relationships'));
  }
  return $form;
}

/**
 * form submission routine to remove user relationships
 */
function coppa_manage_relationships_submit($form, &$form_state) {
  
  foreach ($form_state['values'] as $pid => $child) {
    if (is_numeric($pid)) {
      foreach ($child as $cid => $remove) {
        if ($remove['remove']) {
          db_query('DELETE FROM {coppa} WHERE pid = %d AND cid = %d', $pid, $cid);
        }
      }
    }
  }
}

/**
 * theming function for coppa_manage_relationships
*/
function theme_coppa_manage_relationships($form) {
  $rows = array();
  
  $header = array(t('Parent'), t('Child'), t('Date of Birth'), t('COPPA Status'), t('Delete Relationship'));

  foreach (element_children($form) as $parent) {
    foreach (element_children($form[$parent]) as $child) {
      unset($row);
      $row[] = drupal_render($form[$parent][$child]['parent']);
      $row[] = drupal_render($form[$parent][$child]['child']);
      $row[] = drupal_render($form[$parent][$child]['dob']);
      $row[] = drupal_render($form[$parent][$child]['status']);
      $row[] = drupal_render($form[$parent][$child]['remove']);
      $rows[] = $row;
    }
  }

  $output = theme('table', $header, $rows, array('id' => 'coppa-manage-relationships'));
  $output .= '<br />';
  $output .= drupal_render($form['submit']);

  $output .= drupal_render($form['form_id']);

  $output .= drupal_render($form['form_token']);
  return $output;  
}
