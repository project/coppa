README for coppa.module:

Compatible with Drupal 5.x. Requires the Profile module

This module will prevent users under the age of 13 from logging in if their
respective parent or guardian (P/G) have not "signed" their COPPA compliance 
form. If a users Date of Birth, store in the user profile. If the dob cannot 
be determined the user will not be allowed to login.

P/G to child relationships will be loaded  into an internal {coppa} table. 
This table will track the Parent to Child relationships and monitor when 
COPPA compliance was first achieved.


Suggested setup;
  - enable profile module
  - enable coppa module: this looks for a profile field called "Date of Birth" (profile_dob) and creates it if not present.
  - create a user role that will require COPPA compliance suggestions (student, etc.)
  - visit 'admin/settings/coppa' and select the above user role
  - also visit 'admin/user/coppa/add' to add parent/child relationships
  - also visit 'admin/user/coppa/manage' to manage existing parent/child relationships

Any users with the above user role will now require coppa compliance to login and use the site. Please be careful in choosing the role;  using "authenticated user" can lock all users out of the system if they do not have a profile_dob over 13 years from the current date.